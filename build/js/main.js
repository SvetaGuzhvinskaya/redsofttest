'use strict';

/* eslint-disable no-unused-vars */

var $window = $(window);
var $document = $(document);
var $html = $(document.documentElement);
var $body = $(document.body);

// fixed header 
$window.scroll(function() {
	var $boxFix = $('.js-fix-header');
	if ($(window).scrollTop() > 10) {
		$boxFix.addClass('b-fix-header');
	} else {
		$boxFix.removeClass('b-fix-header');
	}
});

// menu
var $btnMenu = $('.js-btn-menu');
var $boxHamburger = $('.js-hamburger');

$(window).resize(function() {
	if ($(window).width() > 980) {
		$boxHamburger.css('display', 'block');
	} else {
		$boxHamburger.css('display', 'none');
	}
});

function tableFix() {
	$btnMenu.click(function() {
		$boxHamburger.fadeToggle(600);
	});
}
tableFix();

// sub menu
var $subMenu = $('.has-child');
$subMenu.hover(function() {
	clearTimeout($.data(this, 'timer'));
	$('ul', this).stop(true, true).slideDown(200);
}, function() {
	$.data(this, 'timer', setTimeout($.proxy(function() {
		$('ul', this).stop(true, true).slideUp(200);
	}, this), 100));
});
